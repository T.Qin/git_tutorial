This is an example Repository
=============================

You can use [Markdown](https://confluence.atlassian.com/display/STASH038/Markdown+syntax+guide) 
to make a nice readme file for other users which includes

1. ordered lists
2. unordered lists
3. links
4. tables

and so forth. Markdown is designed to look as readable as possible also in text
editors and for basic things it's very easy to use:

| header1    | header2    |
|------------|------------|
| row1, col1 | row1, col2 |
| row2, col1 | row2, col2 |


TODO: add more stuff
